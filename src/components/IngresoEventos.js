import React, { Component } from 'react'
import Validacion from './Validacion';

export default class IngresoEventos extends Component {

    state = {
        error: null
    }

    calcularDef = e => {
        /** Obtención de elementos por name */
        const { elements } = e.target;

        const nombres = this.props.eventos.nombreEvento;

        e.preventDefault();

        /** Obtención de datos */
        const nombreEvento = elements.nombre.value,
            tipoEvento = elements.tipo.value,
            horaInicio = elements.horaInicio.value,
            duracion = elements.duracion.value;

        /** Conversión de hora a minutos */
        let fechaGenerica = new Date("0001-01-01T" + horaInicio);
        let minutos = fechaGenerica.getHours() * 60 + fechaGenerica.getMinutes();
        let nuevaHora = parseInt(minutos) + parseInt(duracion);

        /** Cálculo de la nueva hora */
        let horaFinal = Math.floor(nuevaHora / 60);
        let minutosFinal = nuevaHora % 60;

        if (nombreEvento == '') {
            this.setState({ error: 'Por favor ingrese el nombre del evento' });
        } else if (tipoEvento == '') {
            this.setState({ error: 'Por favor eleccione el tipo del evento' });
        } else if (horaInicio == '') {
            this.setState({ error: 'Por favor ingrese la hora de inicio' });
        } else if (duracion == '') {
            this.setState({ error: 'Por favor ingrese la duración' });
        } else if (nombreEvento == nombres) {
            this.setState({ error: 'Por favor cambie el nombre del evento' });
        } else {
            this.setState({ error: null });
            elements.horaFinal.value = horaFinal + ':' + minutosFinal;
            const horaFin = elements.horaFinal.value;
            const evento = { nombreEvento, tipoEvento, horaInicio, duracion, horaFin };
            this.props.addEvento(evento);
        }
        e.currentTarget.reset();
    }

    render() {
        return (
            <div className="col-md-7 order-md-1">
                <h4 className="d-flex justify-content-between align-items-center mb-3">
                    <span className="text-muted">Ingreso evento</span>
                </h4>
                <form onSubmit={this.calcularDef}>
                    <div className="row">
                        <div className="col-md-6 mb-3">
                            <label>Nombre Evento</label>
                            <input name="nombre" type="text" className="form-control" />
                        </div>
                        <div className="col-md-6 mb-3">
                            <label>Tipo evento</label>
                            <select name="tipo" className="custom-select d-block w-100">
                                <option value="">Seleccione...</option>
                                <option value="Pago">Pago</option>
                                <option value="Gratis">Gratis</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 mb-3">
                            <label>Hora inicio</label>
                            <input name="horaInicio" type="time" className="form-control" />
                        </div>
                        <div className="col-md-6 mb-3">
                            <label>Duración (Minutos)</label>
                            <input name="duracion" type="number" className="form-control" />
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-md-12 mb-3">
                            <label>Hora final</label>
                            <input name="horaFinal" readOnly className="form-control" />
                        </div>
                    </div>
                    <Validacion error={this.state.error} />
                    <hr className="mb-4" />
                    <button className="btn btn-primary btn-lg btn-block bg-purple text-white lh-100" type="submit">
                        Guardar
                    </button>
                </form>
            </div>
        )
    }
}
