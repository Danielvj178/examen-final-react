import React from 'react'

export default function ListarEvento({evento}) {
  console.log(evento);
  return (
    <tr>
        <td>
            {evento.nombreEvento}
        </td>
        <td>
          {evento.horaInicio}
        </td>
        <td>
          {evento.horaFinal}
        </td>
    </tr>
  )
}
