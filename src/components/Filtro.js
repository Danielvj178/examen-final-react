import React, {Component} from 'react';

export default class Filtro extends Component {

    busqueda = (e) => {
        /** Obtención de elementos por name */
        const {elements} = e.target;

        /** Obtención de valores del formulario */
        const opcSeleccionada = elements.filtro.value,
            eventoBuscar = elements.evento.value;

        e.preventDefault();

        this.props.filtro(eventoBuscar, opcSeleccionada);
    }

    render () {return (
        <div className="col-md-5 order-md-2 mb-4">
            <form onSubmit={this.busqueda}>
                <div className="mb-3">
                    <label>Evento</label>
                    <input name="evento" type="text" className="form-control" />
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" className="custom-control-input" value="Pago" id="defaultGroupExample1" name="filtro"/>
                    <label className="custom-control-label" htmlFor="defaultGroupExample1">Pago</label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" className="custom-control-input" value="Gratis" id="defaultGroupExample2" name="filtro"/>
                    <label className="custom-control-label" htmlFor="defaultGroupExample2">Gratis</label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" className="custom-control-input" value="Todos" id="defaultGroupExample3" name="filtro"/>
                    <label className="custom-control-label" htmlFor="defaultGroupExample3">Todos</label>
                </div>
                <hr/>
                <button className="btn btn-primary btn-lg btn-block bg-purple text-white lh-100" type="submit">
                    Buscar
                </button>
            </form>
        </div>
    )}
}