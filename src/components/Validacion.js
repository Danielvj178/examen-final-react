import React from 'react';

export default function Validacion({error}) {
    const clase = ( error ) ? 'alert alert-danger' : '';
    return (
        <div className={clase} role="alert">
            {error}
		</div>
    );
}
