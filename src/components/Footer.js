import React from 'react';

export default function Footer({ name, year }) {
    return (
        <footer className="my-5 pt-5 text-muted text-center text-small" >
            &copy;{name} - {year}
        </footer>
    );
}