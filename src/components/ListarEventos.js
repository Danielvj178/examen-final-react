import React from 'react'
import Listarevento from './ListarEvento';

export default function ListarEventos({ eventos }) {
  console.log(eventos);
  return (
    <div className="col-md-5 order-md-2 mb-4">
      {/* <Badge cantidadCitas={cantidadCitas} /> */}
      <table>
          {eventos.map((evento,i) => <Listarevento
            evento={evento}
            key={i}
          />)}
      </table>
      {/* <ul className="list-group mb-3">
        
      </ul>  */}
    </div>
  )
}
