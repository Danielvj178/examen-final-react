import React from 'react'

export default function validacionBusqueda({error, nombre}) {
    let clase = '';

    if( nombre ){
        clase = 'alert alert-info';
    } else if( !nombre && error ){
        clase = 'alert alert-danger';
    }
  return (
    <div className={clase}>
        {nombre || error}
    </div>
  )
}
