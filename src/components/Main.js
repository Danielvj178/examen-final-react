import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import IngresoEventos from './IngresoEventos';
import BusqInicial from './BusqInicial';
import Filtro from './Filtro';
import ListarEventos from './ListarEventos';

class Main extends Component {
	state = {
		eventos: [],
		eventoBuscar: '',
		opcBuscar: 'Todos'
	}

	addEvento = evento => {
		const eventos = [...this.state.eventos, evento];
		this.setState({ eventos }, () => console.log(eventos));
	}

	filtro = (eventoBuscar, opcBuscar) => {
		this.setState({ eventoBuscar, opcBuscar });
	}

	render() {
		const year = new Date().getFullYear();
		return (
			<div className="container">
				<Header
					titulo="React Workshop Poli"
					year={year}
					autor="Daniel Vidal"
					documento="1152698453"
				/>
				<BusqInicial />
				<hr />
				<div className="row">
					<IngresoEventos
						addEvento={this.addEvento}
						eventos={this.state.eventos}
					/>
					<Filtro
						filtro={this.filtro}
					/>
					<ListarEventos
						eventos={this.state.eventos}
					/>
				</div>
				<Footer
					name="Daniel Vidal"
					year={year}
				/>
			</div>
		);
	}
}

export default Main;
