import React from 'react';
import './Header.css';

export default function Header({ titulo, year, autor, documento }) {
    const style = {
        textAlign: 'center',
      };
    return (
        <header>
            <div className="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm" style={style}>
                <div className="lh-100" style={style}>
                    <h6 className="mb-0 text-white lh-100">{titulo} {year} - {autor} - {documento}</h6>
                </div>
            </div>
        </header>
    );
}
