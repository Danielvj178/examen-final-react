import React, { Component } from 'react'
import ValidacionBusqueda from './ValidacionBusqueda';

export default class BusqInicial extends Component {

  state = {
    nombre: '',
    error: null
  }

  documentoRef = React.createRef();

  getevento = async () => {
    const documento = this.documentoRef.current.value;
    console.log(documento);
    const URL = `https://examen-react-workshop.herokuapp.com/examen/estudiante/${documento}`;

    if( documento ){
      await fetch(URL)
        .then(resp => resp.json())
        .then(data => this.setState({ nombre: data.name, error:null }))
        .catch(error => console.log(error));
      this.setState({error: 'No se encontró'});
    } else {
      this.setState({error:"Por favor ingrese el documento", nombre:null});
    }
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col col-md-2">
              <label>Documento</label>
            </div>
            <div className="col col-md-2">
              <input ref={this.documentoRef} type="text" className="form-control" />
            </div>
            <div className="col col-md-3">
              <button onClick={this.getevento} className="btn btn-primary btn-lg btn-block bg-purple text-white lh-100">
                Buscar
                </button>
            </div>
            <ValidacionBusqueda 
              error={this.state.error}
              nombre={this.state.nombre}
            />
          </div>
          <div>
          </div>
        </div>
      </div>
    )
  }
}
